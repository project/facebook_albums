<?php

/**
 * Settings form
 * @param array $form
 * @param array $form_state
 * @param array $reset
 */
function facebook_albums_settings_form($form, $form_state, $reset = FALSE) {
  $settings = isset($form_state['input']['facebook_albums_settings']) ? $form_state['input']['facebook_albums_settings'] : variable_get('facebook_albums_settings', array());
  $form['facebook_albums_settings'] = array(
    '#tree' => TRUE,
  );

  $form['facebook_albums_settings']['app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#default_value' => isset($settings['app_id']) ? $settings['app_id'] : '',
    '#required' => TRUE,
  );

  $form['facebook_albums_settings']['app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('App secret'),
    '#default_value' => isset($settings['app_secret']) ? $settings['app_secret'] : '',
    '#required' => TRUE,
  );

  if (isset($settings['app_id']) && isset($settings['app_secret']) && $facebook = facebook_albums_facebook_sdk()) {
    // Authenticate Facebook account.
    if ($reset || empty($settings) || !isset($settings['access_token']) || (isset($settings['access_token']) && !facebook_albums_facebook_sdk_is_authenticated($facebook, $settings['access_token']))) {
      if (isset($_GET['code'])) {
        $settings['access_token'] = $facebook->getAccessToken();
        $settings['access_expiry'] = 0;

        variable_set('facebook_albums_settings', $settings);
        drupal_goto('admin/config/services/fbalbumsync');
      } else {
        unset($settings['access_token']);
        unset($settings['access_expiry']);
        variable_set('facebook_albums_settings', $settings);
        if (!isset($_SESSION[md5(serialize($settings))])) {
          $_SESSION[md5(serialize($settings))] = FALSE;
          drupal_goto($facebook->getLoginUrl(array('scope' => array('manage_pages', 'offline_access', 'publish_stream', 'user_photos'))));
        } else {
          unset($_SESSION[md5(serialize($settings))]);
          form_set_error('facebook_albums_settings', 'Authentication failed');
          return system_settings_form($form);
        }
      }
    }
    if (!empty($settings)) {
      $form['facebook_albums_settings']['access_token'] = array(
        '#type' => 'value',
        '#value' => $settings['access_token'],
      );
      $form['facebook_albums_settings']['authenticated'] = array(
        '#type' => 'fieldset',
        '#title' => t('Facebook user'),
        '#description' => theme('facebook_albums_authenticated_user', array(
          'account' => facebook_albums_facebook_sdk_get($facebook, '/me'),
          'access_token' => $settings['access_token'],
          'access_expiry' => $settings['access_expiry'],
        )),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Account config form
 * @param type $form
 * @param type $form_state
 * @return type
 */
function facebook_albums_account_config_form($form, $form_state) {
  $form = array();
  $settings = isset($form_state['input']['facebook_albums_account']) ? $form_state['input']['facebook_albums_account'] : variable_get('facebook_albums_account', array());
  $facebook = facebook_albums_facebook_sdk();

  $form['facebook_albums_account'] = array(
    '#tree' => TRUE,
  );
  $form['facebook_albums_account']['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account settings'),
    '#description' => t('Select account to sync album'),
  );
  $form['facebook_albums_account']['account']['id'] = array(
    '#type' => 'select',
    '#title' => t('Account'),
    '#options' => facebook_albums_account_ids($facebook),
    '#default_value' => isset($settings['account']['id']) ? $settings['account']['id'] : key(facebook_albums_account_ids($facebook)),
    '#required' => TRUE,
  );
  $form['facebook_albums_account']['account']['album'] = array(
    '#type' => 'fieldset',
    '#title' => t('Album settings'),
  );
  $form['facebook_albums_account']['account']['album']['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => array(
      'existing' => t('Existing album'),
    ),
    '#default_value' => isset($settings['account']['album']['type']) ? $settings['account']['album']['type'] : 0,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
