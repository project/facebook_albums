<?php
/**
 * @file
 */
?>
<strong>Name:</strong> <?php echo $account['name']; ?><br />
<strong>Access token:</strong> <?php echo $access_token; ?><br />
<strong>Access expiry:</strong> <?php echo $access_expiry; ?>
<p>
  <?php echo l(t('Re-authenticate'), 'admin/config/services/fbalbumsync/reset') ?>
</p>
